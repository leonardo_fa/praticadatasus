package br.com.leonardo.praticas.mb.pessoa;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class IncluirPessoaMB extends PessoaMB implements Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public String getExtend() {
        return "incluir";
    }

    @Override
    public void preSave() {
        super.preSave();
        if (getModel().getSexo() != null && getModel().getSexo().getId() == 0) {
            getModel().setSexo(null);
        }
    }

}

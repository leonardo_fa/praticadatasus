package br.com.leonardo.praticas.mb;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class SobreMB extends NavegavelMB {

    public List<String> entradas;

    @Override
    public String getAction() {
        return "/sobre/sobre";
    }

    @Override
    public String getMenu() {
        return "sobre";
    }

    public List<String> getEntradas() {
        return Arrays.asList("sobre.label.desenvolvedor", "sobre.label.desenvolvedor.email", "sobre.label.detalhesprojeto");
    }

    public void setEntradas(List<String> entradas) {
        this.entradas = entradas;
    }

}

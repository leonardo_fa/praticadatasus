package br.com.leonardo.praticas.mb;

import static javax.faces.context.FacesContext.getCurrentInstance;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public abstract class NavegavelMB {
    private static final String MENU_ATIVO = "menuAtivo";

    public abstract String getAction();

    public abstract String getMenu();

    public String iniciar() {
        return iniciar(getMenu(), getAction());
    }

    public String iniciar(String menu, String action) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(MENU_ATIVO, menu);
        StringBuilder actionGo = new StringBuilder(action);
        actionGo.append("?faces-redirect=true");
        return actionGo.toString();
    }

    public String getMessageFromI18N(String key) {
        ResourceBundle bundle = ResourceBundle.getBundle("messages_labels", getCurrentInstance().getViewRoot().getLocale());
        return bundle.getString(key);
    }

    public void addMessage(Severity severity, String summary, String detail) {
        if (severity == null) {
            severity = FacesMessage.SEVERITY_INFO;
        }
        if (detail == null) {
            detail = "";
        }
        getCurrentInstance().addMessage(null, new FacesMessage(severity, summary, summary.concat("<br/>").concat(detail)));
    }

}
package br.com.leonardo.praticas.mb;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.hibernate.exception.ConstraintViolationException;

import br.com.leonardo.praticas.model.AbstractEntity;
import br.com.leonardo.praticas.service.AbstractPersistence;

public abstract class CrudNavegavelMB<T extends AbstractEntity> extends NavegavelMB {

    private static final String ID_SELECIONADO = "idSelecionado";
    private static final String SUCESSO_SALVAR = "sucessoSalvar";
    private static final String SUCESSO_REMOVER = "sucessoRemover";

    public CrudNavegavelMB(Class<T> classeEntidade) {
        Boolean sucessoSalvar = (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SUCESSO_SALVAR);
        Boolean sucessoRemover = (Boolean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SUCESSO_REMOVER);

        if (sucessoSalvar != null && sucessoSalvar) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(SUCESSO_SALVAR, null);
            addMessage(FacesMessage.SEVERITY_INFO, getMessageFromI18N("aplicativo.msg.sucesso.salvar"), null);
        }

        if (sucessoRemover != null && sucessoRemover) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(SUCESSO_REMOVER, null);
            addMessage(FacesMessage.SEVERITY_INFO, getMessageFromI18N("aplicativo.msg.sucesso.remover"), null);
        }

        this.classeEntidade = classeEntidade;
    }

    protected Class<T> classeEntidade;

    private T model;

    private List<T> listaFiltrada;

    public abstract AbstractPersistence<T, Number> getService();

    public abstract String getDiretorio();

    public abstract String getLabel();

    public String getActionListar() {
        return "/" + getDiretorio() + "/" + "listar";
    }

    public String getActionEditar() {
        return "/" + getDiretorio() + "/" + "editar";
    }

    public String getMenuListar() {
        return getDiretorio() + "listar";
    }

    public void preSave() {
    }

    public void postSave() {
    }

    public List<T> getListar() {
        return getService().findAll();
    }

    public void novo() {
        try {
            model = classeEntidade.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String carregar(Number idSelecionado) {
        if (idSelecionado != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(ID_SELECIONADO, idSelecionado);
        }
        return iniciar(getMenu(), getActionEditar());
    }

    public String salvar() {
        try {
            preSave();
            getService().save(model);
            postSave();
        } catch (Exception ex) {
            addMessage(FacesMessage.SEVERITY_ERROR, getMessageFromI18N("aplicativo.msg.erro.salvar"), ex.getMessage());
            return "";
        }

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(SUCESSO_SALVAR, true);

        return init();
    }

    public String remover() {
        try {
            getService().remove(model);
        } catch (Exception ex) {
            addMessage(FacesMessage.SEVERITY_ERROR, getMessageFromI18N("aplicativo.msg.erro.remover"), ex.getMessage());
            trataExecaoConstraint(ex);
            return "";
        }

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(SUCESSO_REMOVER, true);

        return init();
    }

    private void trataExecaoConstraint(Throwable throwable) {
        if (throwable != null) {
            if (throwable instanceof ConstraintViolationException) {
                addMessage(FacesMessage.SEVERITY_ERROR, getMessageFromI18N("aplicativo.msg.erro.remover.registrousado"), null);
            } else {
                trataExecaoConstraint(throwable.getCause());
            }
        }

    }

    public String init() {
        return iniciar(getMenuListar(), getActionListar());
    }

    public T getModel() {
        Object objIdSelecionado = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idSelecionado");

        if (objIdSelecionado != null) {
            model = getService().find((Number) objIdSelecionado);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idSelecionado", null);
        }

        if (model == null) {
            novo();
        }

        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public List<T> getListaFiltrada() {
        return listaFiltrada;
    }

    public void setListaFiltrada(List<T> listaFiltrada) {
        this.listaFiltrada = listaFiltrada;
    }

}
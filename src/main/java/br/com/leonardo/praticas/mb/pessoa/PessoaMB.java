package br.com.leonardo.praticas.mb.pessoa;

import java.util.List;

import javax.ejb.EJB;

import br.com.leonardo.praticas.mb.CrudNavegavelMB;
import br.com.leonardo.praticas.model.Pessoa;
import br.com.leonardo.praticas.model.Sexo;
import br.com.leonardo.praticas.service.AbstractPersistence;
import br.com.leonardo.praticas.service.PessoaService;
import br.com.leonardo.praticas.service.SexoService;

public abstract class PessoaMB extends CrudNavegavelMB<Pessoa> {

    @EJB
    private PessoaService service;

    @EJB
    private SexoService sexoService;

    public abstract String getExtend();

    public PessoaMB() {
        super(Pessoa.class);
    }

    @Override
    public AbstractPersistence<Pessoa, Number> getService() {
        return service;
    }

    @Override
    public String getDiretorio() {
        return "pessoa";
    }

    @Override
    public String getAction() {
        return "/" + getDiretorio() + "/" + getExtend();
    }

    @Override
    public String getMenu() {
        return getDiretorio() + getExtend();
    }

    @Override
    public String getLabel() {
        return getMessageFromI18N("pessoa.label");
    }

    private List<Sexo> listaSexos;

    public List<Sexo> getListaSexos() {
        if (listaSexos == null) {
            listaSexos = sexoService.findAll();
        }
        return listaSexos;
    }
}
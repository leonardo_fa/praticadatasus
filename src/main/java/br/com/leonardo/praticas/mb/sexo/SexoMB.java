package br.com.leonardo.praticas.mb.sexo;

import javax.ejb.EJB;

import br.com.leonardo.praticas.mb.CrudNavegavelMB;
import br.com.leonardo.praticas.model.Sexo;
import br.com.leonardo.praticas.service.AbstractPersistence;
import br.com.leonardo.praticas.service.SexoService;

public abstract class SexoMB extends CrudNavegavelMB<Sexo> {

    @EJB
    private SexoService service;

    public abstract String getExtend();

    public SexoMB() {
        super(Sexo.class);
    }

    @Override
    public AbstractPersistence<Sexo, Number> getService() {
        return service;
    }

    @Override
    public String getDiretorio() {
        return "sexo";
    }

    @Override
    public String getAction() {
        return "/" + getDiretorio() + "/" + getExtend();
    }

    @Override
    public String getMenu() {
        return getDiretorio() + getExtend();
    }

    @Override
    public String getLabel() {
        return getMessageFromI18N("sexo.label");
    }
}
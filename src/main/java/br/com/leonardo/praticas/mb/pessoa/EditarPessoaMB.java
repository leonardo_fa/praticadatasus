package br.com.leonardo.praticas.mb.pessoa;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class EditarPessoaMB extends IncluirPessoaMB implements Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public String getExtend() {
        return "editar";
    }

}
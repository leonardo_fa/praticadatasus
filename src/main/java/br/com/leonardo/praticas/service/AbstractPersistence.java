package br.com.leonardo.praticas.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.leonardo.praticas.model.AbstractEntity;

public abstract class AbstractPersistence<T extends AbstractEntity, PK extends Number> {

    private Class<T> entityClass;

    public AbstractPersistence(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public T save(T e) {
        if (e.getId() != null)
            return getEntityManager().merge(e);
        else {
            getEntityManager().persist(e);
            return e;
        }
    }

    public List<T> save(List<T> lista) {
        for (T obj : lista) {
            save(obj);
        }
        return lista;
    }

    @SuppressWarnings("unchecked")
    public void remove(T entity) {
        getEntityManager().remove(find((PK) entity.getId()));
    }

    public T find(PK id) {
        return getEntityManager().find(entityClass, id);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<T> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<T> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    @SuppressWarnings("unchecked")
    public List<T> findQuery(String entidade) {
        StringBuilder query = new StringBuilder("FROM " + entidade + " ent WHERE ent.pai = null order by ent.id");
        return getEntityManager().createQuery(query.toString()).getResultList();
    }

    public List<T> findPrimeiroNivel() {
        return findQuery(entityClass.getSimpleName());
    }

    protected abstract EntityManager getEntityManager();
}

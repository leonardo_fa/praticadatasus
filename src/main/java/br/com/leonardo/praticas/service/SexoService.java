package br.com.leonardo.praticas.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.leonardo.praticas.model.Sexo;

@Stateless
public class SexoService extends AbstractPersistence<Sexo, Number> {

    private static Boolean init = false;

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SexoService() {
        super(Sexo.class);
    }

    @Override
    public List<Sexo> findAll() {
        List<Sexo> lista = super.findAll();
        if (!init && lista.isEmpty()) {
            lista.add(new Sexo("Masculino"));
            lista.add(new Sexo("Feminino"));
            save(lista);
            init = true;
        }
        return lista;
    }

}

package br.com.leonardo.praticas.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.leonardo.praticas.model.Pessoa;

@Stateless
public class PessoaService extends AbstractPersistence<Pessoa, Number> {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PessoaService() {
        super(Pessoa.class);
    }

}

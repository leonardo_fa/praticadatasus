package br.com.leonardo.praticas.model;

import java.io.Serializable;

public interface AbstractEntity extends Serializable {

    public Number getId();

}
